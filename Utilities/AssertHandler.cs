﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
//using System.Linq;
//using System.Xml.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CDW_Example_AllBrowser.Utilities
{
	public class AssertHandler
	{

		public const int STOP_ON_FAIL = 0;
		public const int CONTINUE_ON_FAIL = 1;

		private int intFailedSoftAssertsCount;
		private int intPassedAssertsCount;
		private Dictionary<int, string> dictFailedSoftAssertsErrorMessages = new Dictionary<int, string>();
        private TestContext testContext;

		public AssertHandler(TestContext testContext)
		{

            this.testContext = testContext;
            intFailedSoftAssertsCount = 0;
			intPassedAssertsCount = 0;
		}

		public int FailedSoftAssertsCount
		{
			get
			{
				return intFailedSoftAssertsCount;
			}
		}

        public bool AssertStringContains(string expected, string actual, string errMessage, int hardHaltFlag)
		{
			bool passStatus = true;

            if (hardHaltFlag.Equals(STOP_ON_FAIL))
			{
				//hard halt the test run if fails    
                StringAssert.Contains(actual, expected, errMessage);
                testContext.WriteLine("[StringAssert.Contains] expected: <" + expected + "> actual: <" + actual + ">. Assert is a PASS");

			}
			else
			{
				try
				{
					//continue the test run if fails    
                    StringAssert.Contains(actual, expected, errMessage);
                    //report to testContext 
                    testContext.WriteLine("[StringAssert.Contains] expected: <" + expected + "> actual: <" + actual + ">. Assert is a PASS");

				}
				catch (Exception ex)
				{
					//increment    
					intFailedSoftAssertsCount = intFailedSoftAssertsCount + 1;
					dictFailedSoftAssertsErrorMessages.Add(intFailedSoftAssertsCount, ex.Message);
					passStatus = false;
				}

			}

			return passStatus;

		}

		public bool AssertStringsEqual(string expected, string actual, bool ignoreCase, string errMessage, int hardHaltFlag)
		{
			bool passStatus = true;
            String hardHaltFlagVerbage;

            
            if (hardHaltFlag == 0)
            {

                hardHaltFlagVerbage = "STOP_ON_FAIL";
            }
            else
            {

                hardHaltFlagVerbage = "CONTINUE_ON_FAIL";
            }


			if (hardHaltFlag.Equals(STOP_ON_FAIL))
			{
				//hard halt the test run if fails    
				Assert.AreEqual(expected, actual, ignoreCase, errMessage);
                //report to testContext 
                testContext.WriteLine("[Assert.AreEqual] expected: <" + expected + "> actual: <" + actual + "> ignore case flag: " + ignoreCase + ". hard halt flag: " + hardHaltFlagVerbage + ".");

			}
			else
			{
				try
				{
					//continue the test run if fails    
					Assert.AreEqual(expected, actual, ignoreCase, errMessage);
                    //report to testContext 
                    testContext.WriteLine("[Assert.AreEqual] expected: <" + expected + "> actual: <" + actual + "> ignore case flag: " + ignoreCase + ". hard halt flag: " + hardHaltFlagVerbage + ".");


				}
				catch (Exception error)
				{
					//increment    
					intFailedSoftAssertsCount = intFailedSoftAssertsCount + 1;
					dictFailedSoftAssertsErrorMessages.Add(intFailedSoftAssertsCount, error.Message);
					passStatus = false;
				}

			}

			return passStatus;

		}
                
        public void PrintOutFailedSoftAsserts()
		{
			int indx = 0;

			if (dictFailedSoftAssertsErrorMessages.Count > 0)
			{
				for (indx = 0; indx < dictFailedSoftAssertsErrorMessages.Count; indx++)
				{
					testContext.WriteLine("Assert Error [" + (indx + 1) + "]: " + dictFailedSoftAssertsErrorMessages[indx + 1]);
				}

			}

		}
	}

}