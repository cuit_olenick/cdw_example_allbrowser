﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Windows.Input;
using System.Windows.Forms;
using Microsoft.VisualStudio.TestTools.UITest.Extension;
using Microsoft.VisualStudio.TestTools.UITesting;
using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;
using Microsoft.VisualStudio.TestTools.UITesting.HtmlControls;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CDW_Example_AllBrowser.UIObjects;
using CDW_Example_AllBrowser.Utilities;

namespace CDW_Example_AllBrowser
{
    [CodedUITest()]
    public class TC_01
    {

        private TestContext testContext;
        private BrowserWindow browser;
        private SiteNavigation uISiteNavigation;
        private HomePage uIHomePage;
        private CartPage uICartPage;
        private DealsPage uiDealsPage;
        private HtmlCell cell;
        private ExcelHandler excel;
        private EventHandler<PlaybackErrorEventArgs> errHandler;
        private AssertHandler testAssert;
        private bool bControlExists;
        private string addItemsFilePath;
        private string dataDirectory;
        private string screenShotsFolder;
        private string screenShotsBaseFileName;
        private string projName;
        private string strBrowserName;
        private string cdwHomeUrl;
        private string cdwShoppigCartUrl;
        private string cdwBrowserTitle;
        private string[] arrItemData;
        private Dictionary<int, string> dictColHeadings = new Dictionary<int, string>();
        private Dictionary<int, string> dictAssertErrorMessages = new Dictionary<int, string>();
        private double itemTotals;
        private double itemsCalculatedSubtotals;
        private double itemsAUTSubtotals;
        private int intAssertErrorCounter;

        //<DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", "AddItems.csv", "AddItems#csv", DataAccessMethod.Sequential)>
        [DataSource("System.Data.Odbc",
                    "Dsn=Excel_64Archtctr_for_32Apps;Driver={Microsoft Excel Driver (*.xls, *.xlsx, *.xlsm, *.xlsb)};dbq=|DataDirectory|\\\\TestDriver.xlsx;defaultdir=.;maxbuffersize=2048;pagetimeout=5;readonly=true",
                    "TestDriver$",
                    DataAccessMethod.Sequential),
         DeploymentItem(@"C:\Users\rshulman\Documents\CUIT\CDW_Example_AllBrowser\CDW_Example_AllBrowser\Test Data\TestDriver.xlsx"),
         TestMethod()]
        public void TM_01()
        {

            //String currentDeploymentDirectory = TestContext.DeploymentDirectory;
            //retrieve which window to run test on 
            //strBrowserName = TestContext.DataRow["window"].ToString();
            //BrowserWindow.CurrentBrowser = strBrowserName;
            //TestContext.WriteLine("Browser running: " + BrowserWindow.CurrentBrowser);

            ////launch window window with required url 
            //browser = BrowserWindow.Launch(new Uri(cdwHomeUrl));
            
            //maximize window
            //if (!window.Maximized)
            //{
            //    window.Maximized = true;
            //}

            ////clear window cookies for ie and ff 
            //if (string.Equals(strBrowserName, "iexplorer") || string.Equals(strBrowserName, "firefox"))
            //{
            //    BrowserWindow.ClearCookies();
            //    window.Refresh();
            //    window.WaitForControlReady(30000);
            //}

            //instantiate ui home page operations class 
            uIHomePage = new HomePage(browser, TestContext);
            uIHomePage.PageSync(UIOperations.FIXED_SHORT_MILLISECONDS_TIMEOUT);

            // Navigate to deals page
            uISiteNavigation = new SiteNavigation(browser, TestContext);
            uiDealsPage = uISiteNavigation.clickMenuDeals();
            uiDealsPage.PageSync(UIOperations.FIXED_SHORT_MILLISECONDS_TIMEOUT);

            //assert 'deal of the week' verbage on the page
            testAssert.AssertStringContains("DEAL OF THE WEEK", uiDealsPage.getDealOfTheWeekText(), "Not Shown Deal Of The Week", AssertHandler.CONTINUE_ON_FAIL);
            
            //go to cart page
            uICartPage = uISiteNavigation.clickShoppingCart();
            uICartPage.PageSync(UIOperations.FIXED_SHORT_MILLISECONDS_TIMEOUT);

            //open AddItems.xlsx file 
            excel = new ExcelHandler(addItemsFilePath);

            //retrieve all items to add to cart from item_number column of AddItems.xlsx
            arrItemData = excel.GetAllColumnData("cdw_part_number");


            //iterate through retirved items from AddItems.xlsx and add them to cart
            foreach (string item in arrItemData)
            {
                //syncronise on the cart page
                uICartPage.PageSync(UIOperations.FIXED_SHORT_MILLISECONDS_TIMEOUT);

                //add items to cart
                uICartPage.addItemToCart(item);
            }

            //retrieve CDW cart column headers for added items
            //dictColHeadings = uICartPage.getTableHeadersColumnIndexes(uICartPage.cartItemsTableColHeader);
            uICartPage.verifyItemsAddedToCart(arrItemData);

        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        //' Use TestInitialize to run code before running each test
        [TestInitialize()]
        public void MyTestInitialize()
        {
            //
            cdwHomeUrl = ConfigurationManager.AppSettings.Get("CDWHomePage");
            cdwShoppigCartUrl = ConfigurationManager.AppSettings.Get("CDWShoppingCartPage");
            cdwBrowserTitle = ConfigurationManager.AppSettings.Get("CDWBrowserTitle");

            //turn off smart identification for controls
            //so if they do have changes then the exact match only will suffice 
            //to rectify those changes
            Playback.PlaybackSettings.SmartMatchOptions = SmartMatchOptions.None;
            //The Record and playback engine’s search algorithm has a fail fast logic. 
            //i.e. If the engine is reasonably confident that the search will not succeed, it will return with a failure. 
            //This ensures that you are not waiting for a non-existent control for 2 mins (the default search timeout).
            Playback.PlaybackSettings.ShouldSearchFailFast = true;
            //allows the test to run at full speed. 
            //Disables the waiting for controls to become available to interact with.
            Playback.PlaybackSettings.WaitForReadyLevel = WaitForReadyLevel.Disabled;
            Playback.PlaybackSettings.WaitForReadyTimeout = 30000;
            // Associate an event handler with an event.
            errHandler = Playback_PlaybackError;
            Playback.PlaybackError += errHandler;

            //..\..\src
            projName = Assembly.GetExecutingAssembly().FullName.Split(',')[0];
            string currentDirectory = Directory.GetCurrentDirectory();
            string parentDirectory = currentDirectory.Substring(0, (currentDirectory.IndexOf(projName, System.StringComparison.OrdinalIgnoreCase) + 1 + projName.Length));
            dataDirectory = parentDirectory + projName + "\\Test Data";
            addItemsFilePath = dataDirectory + "\\AddItems.xlsx";
            //AppDomain.CurrentDomain.SetData("DataDirectory", dataDirectory);
            //TestContext.WriteLine("Current Data Directory: " + AppDomain.CurrentDomain.GetData("DataDirectory")); 
            //            TestContext.WriteLine("The current directory is {0}", projName);
            //TestContext.WriteLine("The current directory is {0}", projName);

            screenShotsFolder = "C:\\Users\\rshulman\\Pictures\\Test Run ScreenShots\\";
            screenShotsBaseFileName = "ScreenImage - ";
            testAssert = new AssertHandler(TestContext);

            itemTotals = 0;
            itemsCalculatedSubtotals = 0;
            itemsAUTSubtotals = 0;
            intAssertErrorCounter = 0;


            MessageBox.Show("MyTestInitialize()");


            //retrieve which window to run test on 
            strBrowserName = TestContext.DataRow["browser"].ToString();
            BrowserWindow.CurrentBrowser = strBrowserName;
            TestContext.WriteLine("Browser running: " + BrowserWindow.CurrentBrowser);

            //launch window window with required url 
            browser = BrowserWindow.Launch(new Uri(cdwHomeUrl));

        }

        //' Use TestCleanup to run code after each test has run
        [TestCleanup()]
        public void MyTestCleanup()
        {
            //    '
            //    ' DO NOT USE CODE GENERATOR!!!
            //    '

            if (!(browser == null))
            {
                browser.Close();
                browser = null;
            }

            if (excel != null)
            {
                excel.CloseExcel();
                excel = null;

            }

            //Playback.PlaybackError -= errHandler;


            if (testAssert.FailedSoftAssertsCount > 0)
            {
                testAssert.PrintOutFailedSoftAsserts();
                testAssert = null;
                //MessageBox.Show("MyTestCleanup()");
                Assert.Fail("Assert errors encountered. Check log for error messages.");
            }
            else
            {
                testAssert = null;
            }



        }

        #endregion

        ///<summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContext;
            }
            set
            {
                testContext = value;
            }
        }

        public void Playback_PlaybackError(object sender, PlaybackErrorEventArgs playbackErroEventArgs)
        {
            //This is supported only for the following exceptions:
            //PlaybackFailureException()
            //UITestControlNotFoundException()
            //UITestControlNotAvailableException()
            //TechnologyNotSupportedException()
            //InvalidUITestExtensionPackageException()
            //DecodingFailedException()
            //FailedToPerformActionsOnBlockedControlException()
            //FailedToPerformActionsOnHiddenControlException()
            //UITestControlNotVisibleException()
            //ValidationFailedException()
            //FailedToLaunchApplicationException()
            //ActionNotSupportedOnDisabledControlExcept()

            //check for deal of the week control
            if (uiDealsPage.dealOfTheWeekControlProps.Count > 0)
            {
                if (IfErrorSkip(this.GetSearchPropertiesArray(uiDealsPage.dealOfTheWeekControlProps), playbackErroEventArgs))
                {
                    return;
                }
            } //If uiDealsPage.dealOfTheWeekControlProps.Count > 0 Then

            //check for deal of the week message
            if (uiDealsPage.dealOfTheWeekControlProps.Count > 0)
            {
                if (IfErrorSkip(this.GetSearchPropertiesArray(uiDealsPage.dealOfTheWeekControlProps), playbackErroEventArgs))
                {
                    return;
                }
            } //If uIHomePage.DealOfTheWeekPopupSearchProps.Count > 0 Then

            Playback.PlaybackSettings.ContinueOnError = true;
            playbackErroEventArgs.Result = PlaybackErrorOptions.Retry;

            TestContext.WriteLine("Error Message: " + playbackErroEventArgs.Error.Message);
            TestContext.WriteLine("Exception type: " + playbackErroEventArgs.GetType().FullName);
        }//public void Playback_PlaybackError(object sender, PlaybackErrorEventArgs playbackErroEventArgs)

        public string[] GetSearchPropertiesArray(Dictionary<string, string> searchProperties)
        {
            int loopCounter = 0;
            string[] pairs = null;

            pairs = new string[searchProperties.Count];
            foreach (var item in searchProperties)
            {
                pairs[loopCounter] = item.Key + ":  '" + item.Value + "'";
                loopCounter = loopCounter + 1;
            }

            return pairs;

        }//public string[] GetSearchPropertiesArray(Dictionary<string, string> searchProperties)

        public bool IfErrorSkip(String[] properties, PlaybackErrorEventArgs ex)
        {
            bool found = false;

            foreach (var property in properties)
            {
                if (ex.Error.Message.IndexOf(property, System.StringComparison.OrdinalIgnoreCase) + 1 != 0)
                {
                    Playback.PlaybackSettings.ContinueOnError = true;
                    ex.Result = PlaybackErrorOptions.Skip;
                    TestContext.WriteLine("Error Message:");
                    TestContext.WriteLine(ex.Error.Message);
                    TestContext.WriteLine("Exception type:");
                    TestContext.WriteLine(ex.GetType().FullName);
                    TestContext.WriteLine("");
                    found = true;
                    break;
                }
            }

            return found;

        }//public bool IfErrorSkip(String[] properties, PlaybackErrorEventArgs playbackErroEventArgs)

    }//public class TC_01

}//namespace CDW_Example_AllBrowser