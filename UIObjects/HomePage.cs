﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
//using System.Xml.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Windows.Input;
using Microsoft.VisualStudio.TestTools.UITest.Extension;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UITesting.HtmlControls;

namespace CDW_Example_AllBrowser.UIObjects
{
    public class HomePage
    {

        private TestContext testCotext;
        private HtmlDiv uIDealOfTheWeekPopup;
        private HtmlDiv uIDealOfTheWeekClose;
        private HtmlDiv uIDealOfTheWeekMessage;
        private HtmlControl uISearch;
        private BrowserWindow window;
        private Dictionary<string, string> searchProps;
        private Dictionary<string, string> dictDealOfTheWeekPopupSearchProps = new Dictionary<string, string>();
        private Dictionary<string, string> dictDealOfTheWeekMessageSearchProps = new Dictionary<string, string>();

        /// <summary>
        /// constructor
        /// </summary>
        public HomePage(BrowserWindow browser, TestContext testContext)
        {
            this.window = browser;
            this.testCotext = testCotext;

            searchProps = new Dictionary<string, string>();
            if (!UIOperations.IsClassInitialzed)
            {
                UIOperations.ClassInint(browser, testContext);
            }
        }

 
        /// <summary>
        /// Global Search edit box props
        /// </summary>
        public Dictionary<string, string> globalSearchControlProps
        {
            get
            {
                searchProps.Clear();
                searchProps.Add("Id", "search-input");
                return searchProps;
            }

        }// public Dictionary<string, string> DealOfTheWeekMessageSearchProps

        /// <summary>
        /// Sync on the page
        /// </summary>
        public bool PageSync(int millisecondsTimeout)
        {
            HtmlControl globalSearch = UIOperations.buildHtmlControlUIObject(new HtmlEdit(window), globalSearchControlProps);
            return UIOperations.WaitForControlExist(globalSearch, millisecondsTimeout);
        }

    }//public class UIHomePage

}//namespace CDW_Example_AllBrowser