﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UITesting.HtmlControls;
using System.Text.RegularExpressions;

namespace CDW_Example_AllBrowser.UIObjects
{
    class DealsPage
    {
        private BrowserWindow browser;
        private TestContext testContext;
        private Dictionary<string, string> searchProps;

        /// <summary>
        /// Class constructor
        /// <param name="window">Browser window</param>
        /// <param name="testContext"> TextContext for the test</param>
        /// </summary>
        public DealsPage(BrowserWindow browser, TestContext testContext)
        {
            this.browser = browser;
            this.testContext = testContext;
            if (!UIOperations.IsClassInitialzed)
            {
                UIOperations.ClassInint(browser, testContext);
            }
            searchProps = new Dictionary<string, string>();
        }

        /// <summary>
        /// 'enter email address' control properties
        /// </summary>
        public Dictionary<string, string> enterEmailControlProps
        {
            get
            {
                searchProps.Clear();
                searchProps.Add("Id", "email-input-field");
                return searchProps;
            }

        }

        /// <summary>
        /// 'Deal Of the week' control properties
        /// </summary>
        public Dictionary<string, string> dealOfTheWeekControlProps
        {
            get
            {
                searchProps.Clear();
                searchProps.Add("Id", "spotlight-content");
                return searchProps;
            }

        }

        /// <summary>
        /// 'Deal Of the week' control InnerText value
        /// </summary>
        public string getDealOfTheWeekText()
        {
            HtmlControl dowText = UIOperations.buildHtmlControlUIObject(new HtmlControl(browser), dealOfTheWeekControlProps);
            string controlText = Regex.Replace(dowText.InnerText, @"\r\n?|\n", ">>");
            return controlText;
        }

        /// <summary>
        /// Sync on the page
        /// </summary>
        public bool PageSync(int millisecondsTimeout)
        {
            HtmlEdit emailAddress = UIOperations.buildHtmlEditUIObject(new HtmlEdit(browser), enterEmailControlProps);
            return UIOperations.WaitForControlExist(emailAddress, millisecondsTimeout);
        }
    }
}
