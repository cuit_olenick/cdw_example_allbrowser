﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Xml;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Windows.Input;
using Microsoft.VisualStudio.TestTools.UITest.Extension;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UITesting.HtmlControls;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CDW_Example_AllBrowser.UIObjects;
using System.Reflection;
using CDW_Example_AllBrowser.Utilities;

namespace CDW_Example_AllBrowser.UIObjects
{
	public class CartPage
	{

		
        //private HtmlEdit uITxtQuickCartEdit;
        //private HtmlInputButton uIAddButton;
        //private HtmlDiv uICartPanel;
        //private HtmlTable uICartColHeader;
        //private HtmlTable uICartItems;
        //private HtmlSpan uISubTotal;

        private AssertHandler testAssert;
        private BrowserWindow window;
        private TestContext testContext;
        private MethodBase mBase;
        private Dictionary<string, string> searchProps;

        /// <summary>
        /// Class constructor
        /// <param name="browser">Browser window</param>
        /// <param name="testContext"> TextContext for the test</param>
        /// </summary>
        public CartPage(BrowserWindow browser, TestContext context)
        {
            window = browser;
            testContext = context;
            testAssert = new AssertHandler(context);
            if (!UIOperations.IsClassInitialzed)
            {
                UIOperations.ClassInint(browser, context);
            }
            
            searchProps = new Dictionary<string, string>();
         }

        /// <summary>
        /// 'Add Item Cart' control properties defenition
        /// </summary>
        public Dictionary<string, string> itemCartControlProps
        {
            get
            {
                searchProps.Clear();
                searchProps.Add("Id", "txtQuickCart");
                return searchProps;
            }
        }

        /// <summary>
        /// 'Add' button control properties defenition
        /// </summary>
        public Dictionary<string, string> addButtonControlProps
        {
            get
            {
                searchProps.Clear();
                searchProps.Add("Id", "QuickCart_Button");
                return searchProps;
            }
        }

        /// <summary>
        /// Shoppin Cart page title control properties defenition
        /// </summary>
        public Dictionary<string, string> pageTitleControlProps
        {
            get
            {
                searchProps.Clear();
                searchProps.Add("InnerText", "Shopping Cart");
                return searchProps;
            }

        }

        /// <summary>
        /// item table header control properties defenition
        /// </summary>
        public Dictionary<string, string> itemTableHeaderControlProps
        {
            get
            {
                searchProps.Clear();
                searchProps.Add("Class", "itemHeader");
                return searchProps;
            }

        }
        /// <summary>
        /// item table detail control properties defenition
        /// </summary>
        public Dictionary<string, string> itemTableDetailControlProps
        {
            get
            {
                searchProps.Clear();
                searchProps.Add("Class", "itemTable");
                return searchProps;
            }

        }

        /// <summary>
        /// Sync on the page
        /// </summary>
        public bool PageSync(int millisecondsTimeout)
        {
            HtmlControl pageTitle = UIOperations.buildHtmlControlUIObject(new HtmlControl(window), pageTitleControlProps);
            return UIOperations.WaitForControlExist(pageTitle, millisecondsTimeout);
        }

        /// <summary>
        /// add item to cart
        /// <param name="partNumber">item to add to cart</param>
        /// </summary>
        public bool addItemToCart(string partNumber)
        {
            mBase = MethodBase.GetCurrentMethod();
            bool operationSuccess = false;

            //set part number in add item to cart edit box
            HtmlEdit itemEdit = UIOperations.buildHtmlEditUIObject(new HtmlEdit(window), itemCartControlProps);
            operationSuccess = UIOperations.SetTextEditControl(itemEdit, partNumber);
            if (!operationSuccess)
            {
                testContext.WriteLine("operation was not successful. Control operated on: " + itemEdit.ToString() + "/n" +
                                      "text being applied: " + partNumber + "/n" +
                                      "Executing in {0}.{1}", mBase.ReflectedType.Name, mBase.Name);
                return operationSuccess;
            }

            //click add button
            HtmlControl addButton = UIOperations.buildHtmlControlUIObject(new HtmlControl(window), addButtonControlProps);
            operationSuccess = UIOperations.ClickControl(addButton);
            if (!operationSuccess)
            {
                testContext.WriteLine("operation was not successful. Control operated on: " + addButton.ToString() + "/n" +
                                      "param string text being applied: " + partNumber + "/n" +
                                      "Executing in {0}.{1}", mBase.ReflectedType.Name, mBase.Name);
            }

            return operationSuccess;
        }


        //public HtmlTable cartItemsTableColHeader
        //{
        //    get
        //    {
        //        if ((uICartColHeader == null))
        //        {

        //            uICartColHeader = new HtmlTable(window);
        //            uICartColHeader.SearchProperties.Add(HtmlTable.PropertyNames.Class, "itemHeader");
        //            uICartColHeader.SearchConfigurations.Add(SearchConfiguration.VisibleOnly);
        //            uICartColHeader.SearchConfigurations.Add(SearchConfiguration.AlwaysSearch);
        //        }

        //        return uICartColHeader;
        //    }
        //}//public HtmlTable cartItemsTableColHeader

        //public HtmlTable cartItemsTable
        //{
        //    get
        //    {
        //        if ((uICartItems == null))
        //        {

        //            uICartItems = new HtmlTable(window);
        //            uICartItems.SearchProperties.Add(HtmlTable.PropertyNames.Class, "itemTable");
        //            uICartItems.SearchConfigurations.Add(SearchConfiguration.VisibleOnly);
        //            uICartItems.SearchConfigurations.Add(SearchConfiguration.AlwaysSearch);
        //        }

        //        return uICartItems;
        //    }
        //}//public HtmlTable cartItemsTable

        //public HtmlSpan subTotalText
        //{
        //    get
        //    {
        //        uISubTotal = new HtmlSpan(window);
        //        uISubTotal.SearchProperties.Add(HtmlTable.PropertyNames.Id, "ctl00_Body_cdwCart_CartFooter_lblTotalPrice");

        //        return uISubTotal;
        //    }
        //}//public HtmlSpan subTotalText

        //public object syncController(HtmlControl objUIControl, Int32 millisecondsTimeout)
        //{
        //    bool blSyncSuccessful = false;
        //    try
        //    {
        //        blSyncSuccessful = objUIControl.WaitForControlExist(millisecondsTimeout);
        //    }
        //    catch (Exception playbackErroEventArgs)
        //    {
        //        Console.WriteLine("Error message: " + playbackErroEventArgs.Message);
        //    }


        //    return blSyncSuccessful;

        //}//public object syncController(HtmlControl objUIControl, int millisecondsTimeout)

        //public Dictionary<int, string> getTableHeadersColumnIndexes(HtmlTable uiControl)
        //{
        //    int colCount = 0;
        //    int rowCount = 0;
        //    int rowNum = 0;
        //    int colNum = 0;
        //    HtmlRow row = null;
        //    HtmlCell cell = null;
        //    Dictionary<int, string> columnIndxs = new Dictionary<int, string>();

        //    rowCount = uiControl.RowCount;
        //    for (rowNum = 0; rowNum < rowCount; rowNum++)
        //    {
        //        row = (HtmlRow)uiControl.GetRow(rowNum);
        //        colCount = row.CellCount;
        //        for (colNum = 0; colNum < colCount; colNum++)
        //        {
        //            cell = row.GetCell(colNum);
        //            columnIndxs.Add(colNum, cell.InnerText);
        //        }
        //        //after loading dictionary from the first row get out
        //        break;
        //    }

        //    return columnIndxs;

        //} //public Dictionary<int, string> getTableHeadersColumnIndexes(HtmlTable uiControl)

        /// <summary>
        /// add items to cart
        /// <param name="arrData">string[] item data to add to cart</param>
        /// </summary>
        public bool verifyItemsAddedToCart(string[] arrData)
        {

            int rowCount = 0;
            int recordCount = 0;
            int rowNum = 0;
            //int colNum = 0;
            bool recordFound = false;

            HtmlCell cell = null;
            Dictionary<string, int> cellsValueIndexPairs = new Dictionary<string, int>();

            HtmlTable itemHeader = UIOperations.buildHtmlTableUIObject(new HtmlTable(window),
                                                                        itemTableHeaderControlProps);
            
            cellsValueIndexPairs = UIOperations.getTableRowCellsValueIndexPairs(itemHeader, 0);
            
            //retrieve all the UI controls representing added items to cart (in this case 
            //each item is a HtmlTable control with  item #/description, quantity, availability etc.)
            HtmlTable itemDetails = UIOperations.buildHtmlTableUIObject(new HtmlTable(window), itemTableDetailControlProps);
            UITestControlCollection itemDetailsCollection = UIOperations.getMatchingControlsCollection(itemDetails);

            //iterate through the retireved UI controls and get actual values for each item from AUT
            if (cellsValueIndexPairs.Count > 0)
            {

                foreach (HtmlTable item in itemDetailsCollection)
                {
                    rowCount = item.RowCount;
                    for (rowNum = 0; rowNum < rowCount; rowNum++)
                    {
                        recordFound = false;
                        cell = UIOperations.getTableCellData(item, rowNum, (cellsValueIndexPairs["ITEM"] + 1));
                        string autItem = Regex.Replace(cell.InnerText, @"\r\n?|\n", ">>");
                        for (int i = 0; i < arrData.Length; i++)
                        {
                            if (autItem.Contains(arrData[i]))
                            {
                                recordFound = true;
                                recordCount++;
                                testContext.WriteLine("value '{0}' found in string '{1}'", arrData[i], autItem);
                                break;//for (int i = 0; i < arrItemData.Length; i++)
                            }

                        }
                        
                        //leave for (rowNum = 0; rowNum < rowCount; rowNum++)
                        if (recordFound) break;                         
                    }
                }
            }

            return testAssert.AssertStringsEqual(arrData.Length.ToString(), recordCount.ToString(), false, "Total number of items to add to cart must match.", AssertHandler.CONTINUE_ON_FAIL);
 
        }
        
        public HtmlCell getTableCellData(HtmlTable uiControl, int rowNum, int colNum)
		{
			HtmlRow row = null;
			HtmlCell cell = null;

			row = (HtmlRow)uiControl.GetRow(rowNum);
			if (row.CellCount >= colNum)
			{
				cell = row.GetCell(colNum);
			}
			else
			{
				cell = null;
			}

			return cell;

        } //public HtmlCell getTableCellData(HtmlTable uiControl, int rowNum, int colNum)




    }//public class CartPage

}//namespace CDW_Example_AllBrowser