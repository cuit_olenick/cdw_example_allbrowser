﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UITest.Extension;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Reflection;
using Microsoft.VisualStudio.TestTools.UITesting.HtmlControls;

namespace CDW_Example_AllBrowser.UIObjects
{
    static class UIOperations
    {

        public static readonly int FIXED_VERYLONG_MILLISECONDS_TIMEOUT = 240000;
        public static readonly int FIXED_LONG_MILLISECONDS_TIMEOUT = 60000;
        public static readonly int FIXED_INTERMIDEATE_MILLISECONDS_TIMEOUT = 30000;
        public static readonly int FIXED_SHORT_MILLISECONDS_TIMEOUT = 5000;
        public static readonly int FIXED_ZERO_MILLISECONDS_TIMEOUT = 0;
        
        private static BrowserWindow window;
        private static TestContext testContext;
        private static MethodBase mBase;
        private static bool classInitialized = false; 

        public static bool IsClassInitialzed
        {
            get 
            { 
                return classInitialized;
            }
        }

        [ClassInitialize]
        public static void ClassInint(BrowserWindow browser, TestContext context)
        {
            window = browser;
            testContext = context;
            classInitialized = true;
        }

        ///<summary>
        ///Waits for the control to be ready to accept mouse and keyboard input. 
        ///The engine implicitly calls this API for all actions to wait for the control to be ready before doing any operation. 
        ///However, in certain esoteric scenario, you may have to do explicit call.
        ///</summary>
        public static bool WaitForControlReady(UITestControl uiControl)
        {
            mBase = MethodBase.GetCurrentMethod();

            try
            {
                bool controlReadyStatus = uiControl.WaitForControlReady();
                return controlReadyStatus;

            }
            catch (UITestControlNotFoundException e)
            {
                testContext.WriteLine("Exception has occured. Exception message: " + e.Message + "\n" +
                                      "Control operated on: " + uiControl.ToString()  + "\n" +
                                      "Executing in {0}.{1}", mBase.ReflectedType.Name, mBase.Name);
                return false;
            }
        }

        ///<summary>
        ///Waits for the control to be ready to accept mouse and keyboard input. 
        ///The engine implicitly calls this API for all actions to wait for the control to be ready before doing any operation. 
        ///However, in certain esoteric scenario, you may have to do explicit call.
        ///<summary>
        public static bool WaitForControlReady(UITestControl uiControl, int millisecondsTimeout)
        {
            
            mBase = MethodBase.GetCurrentMethod();
            try 
	        {	        
                bool controlReadyStatus = uiControl.WaitForControlReady(millisecondsTimeout);
                return controlReadyStatus;
		
	        }
            catch (UITestControlNotFoundException e)
            {
                testContext.WriteLine("Exception has occured. Exception message: " + e.Message + "\n" +
                                      "Control operated on: " + uiControl.ToString()  + "\n" +
                                      "Executing in {0}.{1}", mBase.ReflectedType.Name, mBase.Name);
                return false;
            }
        }

        ///<summary>
        ///Waits for the control to be enabled when the wizard is doing some asynchronous validation of the input by making calls to the server. 
        ///For example, you can method to wait for the Next button of the wizard to be enabled (). 
        ///For an example of this method, see Walkthrough: Creating, Editing and Maintaining a Coded UI Test.
        ///<summary>
        public static bool WaitForControlEnabled(UITestControl uiControl)
        {

            mBase = MethodBase.GetCurrentMethod();
            try
            {
                bool controlReadyStatus = uiControl.WaitForControlEnabled();
                return controlReadyStatus;

            }
            catch (UITestControlNotFoundException e)
            {
                testContext.WriteLine("Exception has occured. Exception message: " + e.Message + "\n" +
                                      "Control operated on: " + uiControl.ToString()  + "\n" +
                                      "Executing in {0}.{1}", mBase.ReflectedType.Name, mBase.Name);
                return false;
            }
        }
	
        ///<summary>
        ///Waits for the control to appear on the UI. 
        ///For example, you are expecting an error dialog after the application has done the validation of the parameters. 
        ///The time taken for validation is variable. You can use this method to wait for the error dialog box.
        ///<summary>
        public static bool WaitForControlExist(UITestControl uiControl, int millisecondsTimeout)
        {
            
            mBase = MethodBase.GetCurrentMethod();
            try
            {
                bool controlReadyStatus = uiControl.WaitForControlExist(millisecondsTimeout);
                return controlReadyStatus;

            }
            catch (UITestControlNotFoundException e)
            {
                testContext.WriteLine("Exception has occured. Exception message: " + e.Message + "\n" +
                                      "Control operated on: " + uiControl.ToString()  + "\n" +
                                      "Executing in {0}.{1}", mBase.ReflectedType.Name, mBase.Name);
                return false;
            }
        }

        ///<summary>
        /// Waits for the control to disappear from the UI. For example, you can wait for the progress bar to disappear.
        ///<summary>
        public static bool WaitForControlNotExist(UITestControl uiControl, int millisecondsTimeout)
        {
            mBase = MethodBase.GetCurrentMethod();
            try
            {
                bool controlReadyStatus = uiControl.WaitForControlNotExist(millisecondsTimeout);
                return controlReadyStatus;

            }
            catch (UITestControlNotFoundException e)
            {
                testContext.WriteLine("Exception has occured. Exception message: " + e.Message + "\n" +
                                      "Control operated on: " + uiControl.ToString()  + "\n" +
                                      "Executing in {0}.{1}", mBase.ReflectedType.Name, mBase.Name);
                return false;
            }
        }

       ///<summary>
       ///Waits for the specified property of the control to have the given value. 
       ///For example, you wait for the status text to change to Done.
       ///<summary>
        public static bool WaitForControPropertyEqual(UITestControl uiControl, String propertyName, int millisecondsTimeout)
       {
            mBase = MethodBase.GetCurrentMethod();
            
            try
            {
                bool controlReadyStatus = uiControl.WaitForControlPropertyEqual(propertyName, millisecondsTimeout);
                return controlReadyStatus;

            }
            catch (UITestControlNotFoundException e)
            {
                testContext.WriteLine("Exception has occured. Exception message: " + e.Message + "\n" +
                                      "Control operated on: " + uiControl.ToString()  + "\n" +
                                      "Property to wait for: " + propertyName  + "\n" +
                                      "Executing in {0}.{1}", mBase.ReflectedType.Name, mBase.Name);
                return false;
            }
       }
	
       ///<summary>
       ///Waits for the specified property of the control to have the opposite of a specified value. 
       ///For example, you wait for the edit box to be not read-only, that is, editable.
       ///<summary>
        public static bool WaitForControlPropertyNotEqual(UITestControl uiControl, String propertyName, int millisecondsTimeout)
       {
            mBase = MethodBase.GetCurrentMethod();
            try
            {
                bool controlReadyStatus = uiControl.WaitForControlPropertyNotEqual(propertyName, millisecondsTimeout);
                return controlReadyStatus;
 
            }
            catch (UITestControlNotFoundException e)
            {
                testContext.WriteLine("Exception has occured. Exception message: " + e.Message + "\n" +
                                      "Control operated on: " + uiControl.ToString()  + "\n" +
                                      "Property to wait for: " + propertyName  + "\n" +
                                      "Executing in {0}.{1}", mBase.ReflectedType.Name, mBase.Name);
                return false;
            }
        }

       ///<summary>
       ///'WaitForControlCondition'
       ///Waits for the specified predicate returns to be true. 
       ///This can be used for complex wait operation (like OR conditions) on a given control. 
       ///For example, you can wait until the status text is Succeeded or Failed as shown in the following code:
       /// Define the method to evaluate the condition 
       ///private static bool IsStatusDone(UITestControl control)
       ///{
       ///    WinText statusText = (WinText)control;
       ///    return statusText.DisplayText == "Succeeded" || statusText.DisplayText == "Failed";
       ///} 
       /// In test method, wait till the method evaluates to true 
       ///statusText.WaitForControlCondition(IsStatusDone);

       ///'WaitForCondition<T>'
       ///All the previous methods are instance methods of UITestControl. 
       ///This method is a static method. This method also waits for the specified predicate to be true 
       ///but it can be used for complex wait operation (like OR conditions) on multiple controls. 
       ///For example, you can wait until the status text is Succeeded or until an error message appears, as shown in the following code:
       ///
       /// Define the method to evaluate the condition 
       ///private static bool IsStatusDoneOrError(UITestControl[] controls) 
       ///{ 
       ///    WinText statusText = (WinText)controls[0]; 
       ///    WinWindow errorDialog = (WinWindow)controls[1]; 
       ///    return statusText.DisplayText == "Succeeded" || errorDialog.Exists; 
       ///} 
       ///
       /// In test method, wait till the method evaluates to true 
       ///UITestControl.WaitForCondition<UITestControl[]>(new UITestControl[] { statusText, errorDialog }, IsStatusDoneOrError); 
       ///<summary>

       ///<summary>
       ///Build HtmlControl control
       /// <param name="uiControl">HtmlControl control to build.</param>
       /// <param name="searchProps">Dictionary of property names/values pairs to build the control on.</param>
       ///<summary>
        public static HtmlControl buildHtmlControlUIObject(HtmlControl uiControl, Dictionary<string, string> searchProps)
       {
           mBase = MethodBase.GetCurrentMethod();

           foreach (var key in searchProps.Keys)
            {

                switch (key)
                {
                    case "Id":
                        uiControl.SearchProperties.Add(HtmlControl.PropertyNames.Id, searchProps[key]);
                        break;
                    
                    case "InnerText":
                        uiControl.SearchProperties.Add(HtmlControl.PropertyNames.InnerText, searchProps[key]);
                        break;

                    default:
                        break;
                }
            }

            return uiControl;

 
       }

       ///<summary>
       ///Build HtmlButton control
       /// <param name="uiControl">HtmlButton control to build.</param>
       /// <param name="searchProps">Dictionary of property names/values pairs to build the control on.</param>
       ///<summary>
        public static HtmlButton buildHtmlButtonUIObject(HtmlButton uiControl, Dictionary<string, string> searchProps)
       {

           mBase = MethodBase.GetCurrentMethod();

           foreach (var key in searchProps.Keys)
           {

               switch (key)
               {
                   case "Id":
                       uiControl.SearchProperties.Add(HtmlButton.PropertyNames.Id, searchProps[key]);
                       break;

                   default:
                       break;
               }
           }

           return uiControl;
       }

       ///<summary>
       ///Build HtmlHyperlink control
       /// <param name="uiControl">HtmlHyperlink control to build.</param>
       /// <param name="searchProps">Dictionary of property names/values pairs to build the control on.</param>
       ///<summary>
        public static HtmlHyperlink buildHtmlHyperlinkUIObject(HtmlHyperlink uiControl, Dictionary<string, string> searchProps)
       {
           mBase = MethodBase.GetCurrentMethod();

           foreach (var key in searchProps.Keys)
           {

               switch (key)
               {
                   case "Id":
                       uiControl.SearchProperties.Add(HtmlHyperlink.PropertyNames.Id, searchProps[key]);
                       break;
                   
                   case "InnerText":
                       uiControl.SearchProperties.Add(HtmlHyperlink.PropertyNames.InnerText, searchProps[key]);
                       break;

                   default:
                       break;
               }
           }

           return uiControl;
       }

       ///<summary>
       ///Build HtmlEdit control
       /// <param name="uiControl">HtmlEdit control to build.</param>
       /// <param name="searchProps">Dictionary of property names/values pairs to build the control on.</param>
       ///<summary>
        public static HtmlEdit buildHtmlEditUIObject(HtmlEdit uiControl, Dictionary<string, string> searchProps)
       {
           mBase = MethodBase.GetCurrentMethod();

           foreach (var key in searchProps.Keys)
           {

               switch (key)
               {
                   case "Id":
                       uiControl.SearchProperties.Add(HtmlEdit.PropertyNames.Id, searchProps[key]);
                       break;

                   default:
                       break;
               }
           }

           return uiControl;
       }

       ///<summary>
       ///Build HtmlTable control
       /// <param name="uiControl">HtmlTable control to build.</param>
       /// <param name="searchProps">Dictionary of property names/values pairs to build the control on.</param>
       ///<summary>
        public static HtmlTable buildHtmlTableUIObject(HtmlTable uiControl, Dictionary<string, string> searchProps)
       {
           mBase = MethodBase.GetCurrentMethod();

           foreach (var key in searchProps.Keys)
           {

               switch (key)
               {
                   case "Id":
                       uiControl.SearchProperties.Add(HtmlTable.PropertyNames.Id, searchProps[key]);
                       break;

                   case "Class":
                       uiControl.SearchProperties.Add(HtmlTable.PropertyNames.Class, searchProps[key]);
                       break;

                   default:
                       break;
               }
           }

           return uiControl;
       }

       ///<summary>
       ///ClickControl on control
       /// <param name="uiControl">HtmlControl control to operate on.</param>
       ///<summary>
        public static bool ClickControl(UITestControl uiControl)
       {
           mBase = MethodBase.GetCurrentMethod();
           try
           {
               Mouse.Click(uiControl);
               return true;
           }
           catch (Exception e)
           {

               testContext.WriteLine("Exception has occured. Exception message: " + e.Message + "\n" +
                                     "Control operated on: " + uiControl.ToString() + "\n" +
                                     "Executing in {0}.{1}", mBase.ReflectedType.Name, mBase.Name);
               return false;
           }
       }


       ///<summary>
       ///set controls text
       /// <param name="uiControl">UITestControl control to operate on.</param>
       /// <param name="text">string text to set.</param>
       ///<summary>
        public static bool SetTextEditControl(HtmlEdit uiControl, string text)
       {
           mBase = MethodBase.GetCurrentMethod();
           try
           {
               uiControl.Text = text;
               return true;
           }
           catch (Exception e)
           {

               testContext.WriteLine("Exception has occured. Exception message: " + e.Message + "\n" +
                                     "Control operated on: " + uiControl.ToString() + "\n" +
                                     "Executing in {0}.{1}", mBase.ReflectedType.Name, mBase.Name);
               return false;
           }
       }

       ///<summary>
       ///get column indexes and related values for the row from each cell
       /// <param name="uiControl">HtmlTable control to operate on.</param>
       /// <param name="rowNum">row number to postion table to.</param>
       ///<summary>
        public static Dictionary<int, string> getTableRowCellsIndexValuePairs(HtmlTable uiControl, int rowNum)
       {
           int colCount = 0;
           int colNum = 0;
           HtmlRow row = null;
           HtmlCell cell = null;
           Dictionary<int, string> cellsIndexValuePairs = new Dictionary<int, string>();
            
           try
           {
                row = (HtmlRow)uiControl.GetRow(rowNum);
                colCount = row.CellCount;
                for (colNum = 0; colNum < colCount; colNum++)
                {
                    cell = row.GetCell(colNum);
                    cellsIndexValuePairs.Add(colNum, cell.InnerText);
                }
  
                return cellsIndexValuePairs;

           }
           catch (Exception e)
           {
               testContext.WriteLine("Exception has occured. Exception message: " + e.Message + "\n" +
                                    "Control operated on: " + uiControl.ToString() + "\n" +
                                    "table row num param: " + rowNum + "\n" +
                                    "Executing in {0}.{1}", mBase.ReflectedType.Name, mBase.Name);
 
               return cellsIndexValuePairs;
           }
       }

       ///<summary>
       ///get column indexes and related values for the row from each cell
       /// <param name="uiControl">HtmlTable control to operate on.</param>
       /// <param name="rowNum">row number to postion table to.</param>
       ///<summary>
        public static Dictionary<string, int> getTableRowCellsValueIndexPairs(HtmlTable uiControl, int rowNum)
       {
           int colCount = 0;
           int colNum = 0;
           int incrementCount = -1;
           string tempVal = "";
           HtmlRow row = null;
           HtmlCell cell = null;
           Dictionary<string,int> cellsIndexValuePairs = new Dictionary<string, int>();

           try
           {
               row = (HtmlRow)uiControl.GetRow(rowNum);
               colCount = row.CellCount;
               for (colNum = 0; colNum < colCount; colNum++)
               {
                   cell = row.GetCell(colNum);
                   tempVal = (cell.InnerText).Trim();
                   if (String.IsNullOrEmpty(tempVal) || String.IsNullOrWhiteSpace(tempVal))
                   {
                        cellsIndexValuePairs.Add("BLANK_" + (++incrementCount), colNum);
                       
                   }
                   else
                       cellsIndexValuePairs.Add(tempVal.ToUpper(), colNum);
               }

               return cellsIndexValuePairs;

           }
           catch (Exception e)
           {
               testContext.WriteLine("Exception has occured. Exception message: " + e.Message + "\n" +
                                    "Control operated on: " + uiControl.ToString() + "\n" +
                                    "table row num param: " + rowNum + "\n" +
                                    "Executing in {0}.{1}", mBase.ReflectedType.Name, mBase.Name);

               return cellsIndexValuePairs;
           }
       }



        public static UITestControlCollection getMatchingControlsCollection(UITestControl uiControl)
       {

           mBase = MethodBase.GetCurrentMethod();
           try
           {
               return uiControl.FindMatchingControls();
           }
           catch (Exception e)
           {

               testContext.WriteLine("Exception has occured. Exception message: " + e.Message + "\n" +
                                     "Control operated on: " + uiControl.ToString() + "\n" +
                                     "Executing in {0}.{1}", mBase.ReflectedType.Name, mBase.Name);
               return null;
           }

       }

        public static HtmlCell getTableCellData(HtmlTable uiControl, int rowNum, int colNum)
       {
           mBase = MethodBase.GetCurrentMethod();
           HtmlRow row = null;
           HtmlCell cell = null;

           try
           {
               row = (HtmlRow)uiControl.GetRow(rowNum);
               if (row.CellCount >= colNum)
               {
                   cell = row.GetCell(colNum);
               }
               else
               {
                   cell = null;
               }

               return cell;

           }
           catch (Exception e)
           {

               testContext.WriteLine("Exception has occured. Exception message: " + e.Message + "\n" +
                                     "Control operated on: " + uiControl.ToString() + "\n" +
                                     "Parameters rowNum, colNum {0},{1}: " + rowNum, colNum + "\n" +
                                     "Executing in {0}.{1}", mBase.ReflectedType.Name, mBase.Name);
               return null;
           }
       }
    }
}

