﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UITesting.HtmlControls;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CDW_Example_AllBrowser.UIObjects;

namespace CDW_Example_AllBrowser.UIObjects
{
    class SiteNavigation {

        private TestContext testContext;
        private BrowserWindow browser;
        private Dictionary<string, string> searchProps;

        public SiteNavigation(BrowserWindow browser, TestContext testContext)
        {
            this.browser = browser;
            this.testContext = testContext;
            searchProps = new Dictionary<string, string>();
            if (!UIOperations.IsClassInitialzed)
            {
                UIOperations.ClassInint(browser, testContext);
            }

        }

        /// <summary>
        /// Global Search Control properties
        /// </summary>
        public Dictionary<string, string> globalSiteSearchControlProperties
        {
            get
            {
                searchProps.Clear();
                searchProps.Add("Id", "search-input");
                return searchProps;
            }

        }

        /// <summary>
        /// menu 'Deals' control properties
        /// </summary>
        public Dictionary<string, string> menuDealsControlProps
        {
            get
            {
                searchProps.Clear();
                searchProps.Add("InnerText", "Deals");
                return searchProps;
            }

        }

        /// <summary>
        /// menu 'Deals' control properties
        /// </summary>
        public Dictionary<string, string> shoppingCartControlProps
        {
            get
            {
                searchProps.Clear();
                searchProps.Add("Id", "button-cart");
                return searchProps;
            }

        }

        /// <summary>
        /// click 'Deals' menu item
        /// </summary>
        public DealsPage clickMenuDeals()
        {
            HtmlHyperlink deals = UIOperations.buildHtmlHyperlinkUIObject(new HtmlHyperlink(browser), menuDealsControlProps);
            bool operationSuccess = UIOperations.ClickControl(deals);
            if (operationSuccess)
            {
                return new DealsPage(browser, testContext);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// click on 'Shopping Cart'
        /// </summary>
        public CartPage clickShoppingCart()
        {
            HtmlHyperlink cart = UIOperations.buildHtmlHyperlinkUIObject(new HtmlHyperlink(browser), shoppingCartControlProps);
            bool operationSuccess = UIOperations.ClickControl(cart);
            if (operationSuccess) 
            {
                return new CartPage(browser, testContext);
            } 
            else 
            { 
                return null;
            }
        }

    }
}
